

/*Dainius bando tapti zombiu su chartais*/

var ctx = document.getElementById('zombieChart').getContext('2d');
var boxChart=new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ["I ketv.", "II ketv.", "III ketv.", "IV ketv."],
      datasets: [
        {
          label: "Sunaikintas zombių konkurentų skaičius per ketvirtį",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
          data: [42,50,32,55]
        }
      ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
          },
      legend: { display: false },
      title: {
        display: true,
        text: 'Sunaikintų konkurentų zombių skaičius, 2017 m.'
              }
    }
});


var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
}