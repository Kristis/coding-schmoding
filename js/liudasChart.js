function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Ieškau smegenų', 'Val per diena'],
          ['Tiesiog stoviu ir nieko neveikiu',     11],
          ['Valgau',      2],
          ['Ieškau žmonių',  2],
          ['Bandau pagauti žmogų', 2],
          ['Tiesiog stoviu',    7]
        ]);

        var options = {
          title:'Mano dienos veikla',
          is3D: true,

          
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1} 
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none"; 
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block"; 
  dots[slideIndex-1].className += " active";
} 