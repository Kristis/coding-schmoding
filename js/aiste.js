var stabdis = false;
$(".dropdown-trigger").dropdown();
$(".myContainer").mouseenter(function() {
    stabdis = true;
});

$(".myContainer").mouseleave(function() {
    stabdis = false;
});



/* Aistės chart pradžia*/
var ctx = document.getElementById("myChartAiste").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Dykumoje", "Laukinėje gamtoje", "Cunamio metu", "Žemės drebėjimo metu", "Zombių apokalipsė",],
        datasets: [{
            label: 'Asmens gebėjimas išgyventi',
            data: [3, 30, 53, 49, 0],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});


var ctx = document.getElementById('ChartAiste').getContext('2d');
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'pie',

    // The data for our dataset
    data: {
        labels: ["1", "2", "3", "4" ],
        datasets: [{
            label: "Random",
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
               
                ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235,1)',
                'rgba(255, 206, 86,1)',
                'rgba(75, 192, 192,1)',
                
            ],
            data: [4, 20, 30, 45],
        }]
    },

    // Configuration options go here
    options: {}
});

/*Aistės Chart pabaiga*/

/*Karuselė*/


/*Karuselės pabaiga*/