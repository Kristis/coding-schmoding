
var stabdis = false;
$(".dropdown-trigger").dropdown();
$(".myContainer").mouseenter(function() {
	stabdis = true;
});

$(".myContainer").mouseleave(function() {
	stabdis = false;
});

//cards "about us" js veikia geras
   $(document).ready(function(){
     $('.tabs').tabs();
   });
  $(document).ready(function(){
    $('.carousel').carousel();

  });
/* Karuseles aktyvavimas*/
setTimeout(autoplay, 3000);
function autoplay() {
    if(!stabdis) {
    	$('#karusele').carousel('next');
	}
	setTimeout(autoplay, 5000);
}
////////////////////////mapso kelimas//
/* Google Map outside of tabs */
        function init() {
          // Google Map Options
          var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 15,
            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(54.711757, 25.291465), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            styles: [{
              "featureType": "administrative.country",
              "elementType": "geometry",
              "stylers": [{
                "visibility": "simplified"
              }, {
                "hue": "white"
              }]
            }]
          };

          // Get the HTML DOM element that will contain your map
          // div id="map" 
          var mapElement2 = document.getElementById('map_2');

          // Create the Google Map using our element and options defined above
          var map = new google.maps.Map(mapElement2, mapOptions);

          /* Providence Marker */
          var marker1 = new google.maps.Marker({
            position: new google.maps.LatLng(54.711757, 25.291465),
            map: map,
            title: 'Somewhere around here!'
          });

        }
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);
        //google.maps.event.addDomListener(window, 'load', initTabMap);




// Liudo charto funkcija
function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Ieškau smegenų', 'Val per diena'],
          ['Tiesiog stoviu ir nieko neveikiu',     11],
          ['Valgau',      2],
          ['Ieškau žmonių',  2],
          ['Bandau pagauti žmogų', 2],
          ['Tiesiog stoviu',    7]
        ]);

        var options = {
          title:'Mano dienos veikla',
          is3D: true,

          
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }

 
  $(document).ready(function(){
    $('.carousel').carousel();
  });

setTimeout(autoplay, 3000);
function autoplay() {
    $('.carousel').carousel('next');
    setTimeout(autoplay, 5000);
 
}


//ruta bando
Chart.defaults.global.defaultFontColor = '#3e2723';
Chart.defaults.global.defaultFontFamily = 'Helvetica';
Chart.defaults.global.defaultFontSize = 16;
Chart.defaults.global.title.position='bottom';



var ctx = document.getElementById('lineChartRuta').getContext('2d');
let chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'doughnut',

    // The data for our dataset
    data: {
        labels: ["Vienas", "Du", "Trys", "Keturi", "Panki", "Sesi", "Septyni", "Aštuoni","Devyni","Dešimt"],
        datasets: [{

            label: "Svarbūs skaičiai",
            backgroundColor:  ['#3e2723','#4e342e','#5d4037','#795548','#795548','#8d6e63','#a1887f','#bcaaa4','#d7ccc8','#efebe9'],
            borderColor: '#795548',
            data: [30, 15, 25, 10, 30, 15,45, 15, 55,20],
        }, {
               
        }]
    },

    // Configuration options go here
    options: {
      cutoutPercentage: 20,
    }
});

var ctx = document.getElementById('miksuotas').getContext('2d');
var miksas= new Chart(ctx,{
type: 'bar',
  data: {
    datasets: [{
          label: 'Grynasis pelnas',

          data: [30, 15, 25, 10, 30, 15,45, 15, 55,20],
          backgroundColor: ['#3e2723','#4e342e','#5d4037','#795548','#795548','#8d6e63','#a1887f','#bcaaa4','#d7ccc8','#efebe9'],
                
        }, {
          label: 'Nuostolis',
          data: [20, 45, 13, 36,30, 15, 25, 10, 30, 15],

          backgroundColor: ['#fbe9e7'],

          backgroundColor: ['white'],


          // Changes this dataset to become a line
          type: 'line'
        }],
    labels: ["Vienas", "Du", "Trys", "Keturi", "Panki", "Sesi", "Septyni", "Aštuoni","Devyni","Dešimt"]
  },
  options: {}
});

//ruta baige charts

